## Follow the steps


```
npm install -g sequelize-cli
```
                            
1) Create mysql database named ``` justclean ```
2) If you have local database under password protection set connections settings 
 
```
config/config.json
```


3) Run this command to create tables 
```
sequelize db:migrate
```

4) install npm 
```
npm install
```

5) start Vue
6) webpack development server will start at: http://localhost:3000

```
npm start
```

6) You also need to start the NodeJS server by typing following command.

```
nodemon server
```





