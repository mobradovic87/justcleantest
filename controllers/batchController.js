var fs = require('fs');
var csv = require("fast-csv");
var batch = require('../models/batch');
var user = require('../controllers/userController');
const sequelize = require('../models');
const Promise = require('bluebird');

module.exports = {
    parse:function (file) {
        let users =[];
        var stream = fs.createReadStream(file.path);
        let count = 1;
        csv.fromStream(stream, {headers : ["id","name","email","phone","image_link","title"], ignoreEmpty: true})
            .on("data", data =>{
                users.push(data);
            }).on("end", () => {
                this.saveToDatabase(file.name,users.length,true,false);
                user.saveUserToTable(users.slice(1));
            }).on("error", data =>{
                if(count == 1){
                    this.saveToDatabase(file.name, 0, false, true);
                }
                count++;
                    return false;
            });

    },
    saveToDatabase:function (fileName,length,completed,error) {
        return sequelize.Batch.create({
            name: fileName,
            number_of_entries: length,
            completed: completed,
            incomplete_error: error
        }).then(batch =>{
            console.log('Batch saved to database : ' +batch.id);
        });
    },
    getBatchData:function (req,res) {
        let query =null;
        const Op = sequelize.Op;
        if(req.query.from || req.query.to){
                query = {
                    where: {
                        createdAt: {
                            $gte: req.query.from,
                            $lte: req.query.to
                        }
                    }
                };
        }else{
                query = {order : [[req.query.sort,req.query.order]]};
        }
        return sequelize.Batch.findAll(query).then(batches => {
            res.json(batches);
        });
    },


}