var fs = require('fs');
const sequelize = require('../models');
const Promise = require('bluebird');
var url = require('url');
var http = require('https');
const google = require('googleapis');
var https = require('follow-redirects').https;
const queryString = require('query-string');
module.exports = {

    getUserData:function (req,res) {

        let query = {order : [[req.query.sort,req.query.order]]};
        return sequelize.Users.findAll(query).then(users => {
            res.json(users);
        });
    },

    downloadImages: function (url_path,email,userID) {

        let parsed = queryString.parseUrl(url_path);
        var dir = '/images/users/';
        var imageName = dir+userID+Date.now()+'.jpg';
        var file = fs.createWriteStream('.'+imageName);
        sequelize.Images.create({path:imageName,user_id:userID});
        var request = https.get("https://drive.google.com/uc?export=download&id="+parsed.query.id, function(response) {
            response.pipe(file);
        });
        this.updateUserImage(imageName,userID);

    },
    updateUserImage(imagePath,userId) {
        return sequelize.Users.update({
            image_link: 'http://localhost:3000'+ imagePath,
        }, { where: { id: userId }})
            .spread((updated, user) =>{
                return user;
            });
    },
    saveUserToTable:function (data) {

        return Promise.resolve(data)
            .map(row => {
                return sequelize.Users.findOne({ email: row['email'] })
                    .then(user =>{
                        if(!user) {
                            console.log('creating user');
                            return sequelize.Users.create({
                                name: row['name'],
                                phone: row['phone'],
                                email: row['email'],
                                image_link: row['image_link'],
                                title: row['title']
                            }).then(user =>{
                                this.downloadImages(row['image_link'],row['email'],user.get('id'));
                            });
                        } else {
                            if(user.email == row['email'] &&
                                user.image_link != row['image_link']){
                                this.downloadImages(row['image_link'],row['email'],user.get('id'));
                            }
                            console.log('updating user');
                            return sequelize.Users.update({
                                name: row['name'],
                                phone: row['phone'],
                                email: row['email'],
                                title: row['title']
                            }, { where: { email: row['email'] }})
                                .spread((updated, user) =>{
                                return user;
                                });
                        }
                    })
                    .then(function (user) {
                        //console.log(user);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            });
    }
}