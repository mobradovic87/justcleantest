var express = require('express');
var app = express();
var itemRoutes = express.Router();
var batchController = require('../controllers/batchController');
var userController = require('../controllers/userController');
var formidable = require('formidable');

var fileName = null;
itemRoutes.post('/upload', function(req, res) {

    var form = new formidable.IncomingForm();
    form.multiples = true;

    form.on('file', function(field, file) {

        fileName = file.name;
        batchController.parse(file);
    });

    form.on('error', function(err) {

        batchController.saveToDatabase(fileName,0,false,true);
        res.sendStatus(500);
    });

    form.on('end', function() {

         res.sendStatus(200);

    });

    form.parse(req);



});
itemRoutes.get('/get-batch',batchController.getBatchData);
itemRoutes.get('/get-users',userController.getUserData);



module.exports = itemRoutes;
