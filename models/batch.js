'use strict';
module.exports = (sequelize, DataTypes) => {
  var Batch = sequelize.define('Batch', {
    name: DataTypes.STRING,
    number_of_entries: DataTypes.STRING,
    completed: DataTypes.BOOLEAN,
    incomplete_error: DataTypes.BOOLEAN,
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Batch;
};