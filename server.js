const express = require('express'),
      path = require('path'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      routes = require('./expressRoutes/routes'),
      Sequelize = require('sequelize');

      const app = express();
      app.use(express.static('public'));
      app.use(bodyParser.json());
      app.use(cors());
      app.use('/api/v1', routes);
      const port = process.env.PORT || 4000;


      const sequelize = new Sequelize('justclean', 'root', '', {
          host: 'localhost',
          dialect: 'mysql',

          pool: {
              max: 5,
              min: 0,
              acquire: 30000,
              idle: 10000
          },
      });

      sequelize
          .authenticate()
          .then(() => {
              console.log('Connection has been established successfully.');
          })
          .catch(err => {
              console.error('Unable to connect to the database:', err);
          });



      const server = app.listen(port, function(){
        console.log('Listening on port ' + port);
      });
