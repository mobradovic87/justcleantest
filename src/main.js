import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import UploadBatch from './components/UploadBatch.vue';
import DataPreview from './components/DataPreview.vue';
import BatchPreview from './components/BatchPreview.vue';

import Datatable from 'vue2-datatable-component';

Vue.use(Datatable);
const routes = [

        {
          name: 'DisplayItem',
          path: '/',
          component: UploadBatch
      },{
          name: 'Data',
          path: '/data-preview',
          component: DataPreview
      },{
          name: 'Batch',
          path: '/batch-preview',
          component: BatchPreview
      },

];

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount('#app');
